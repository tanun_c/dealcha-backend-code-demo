@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">

          <table class="table default compact">
            <thead>
              @foreach($conversion_stat as $key => $value)
              <th>{{ $value['day'] }}</th>
              @endforeach
            </thead>

            <tr>
              @foreach($conversion_stat as $key => $value)
              <td>{{ $value['count'] }}</td>
              @endforeach
            </tr>
          </table>

        </div>
      </div>
    </div>
  </div>

  <pre>
  {{ var_dump($conversion_stat) }}
  </pre>
@endsection