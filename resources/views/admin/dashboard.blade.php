@extends('layouts.app')

@section('content')
<div class="container">

  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="row">
        @foreach($single_reports as $report)
        <div class="col-md-3">
          <div class="panel panel-default">
            <div class="panel-heading"><small>{!! $report['heading'] !!}</small></div>
            <div class="panel-body text-right">{{ $report['amount'] }}</div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">

        <table class="table table-striped table-condensed small">
          <thead>
            <th class="text-right">Month</th>
            <th class="text-right">#trips</th>
            <th class="text-right">#users</th>
            <th class="text-right">#CASHBACK</th>
            <th class="text-right">pending</th>
            <th class="text-right">approved</th>
            <th class="text-right">#REVENUE</th>
            <th class="text-right">pending</th>
            <th class="text-right">approved</th>

            <th class="text-right">BONUS</th>
            <th class="text-right">PAYOUT</th>
          </thead>
          <tbody>
            @foreach($months_number_data as $month)
            <tr>
              <td class="text-right">
                <a href="{{ $month['href'] }}">{{ $month['month'] }}</a>
              </td>
              <td class="text-right">{{ $month['trips'] }}</td>
              <td class="text-right">{{ $month['users'] }}</td>
              <td class="text-right">{{ $month['cashback'] }}</td>
              <td class="text-right">{{ $month['cb_pending'] }}</td>
              <td class="text-right">{{ $month['cb_approved'] }}</td>
              
              
              <td class="text-right">{{ $month['revenue'] }}</td>
              <td class="text-right">{{ $month['revenue_pending'] }}</td>
              <td class="text-right">{{ $month['revenue_approved'] }}</td>
              
              <td class="text-right">{{ $month['bonus'] }}</td>
              <td class="text-right">{{ $month['payout'] }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      
    </div>
  </div>

  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default small">
        <div class="row">
          <div class="col-xs-10 col-xs-offset-1 text-center">
            <br>
            <a href="/admin/dashboard">All</a>

            @foreach($stores as $store)
             | 
            <a href="/admin/dashboard?store={{$store->id}}">{{ $store->name}}</a>
            @endforeach
            <br><br> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
