@extends('layouts.app')

@section('content')
<div class="container">

  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <a href="/admin/dashboard">
        <small>Dashboard</small>
      </a> | 
      
      <a href="/admin/dashboard/{{$month_start->format("Y")}}/{{strtolower($month_start->format("M"))}}">
        <small>{{ $month_start->format("M Y") }}</small>
      </a>
    </div>
  </div>
  
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <h2>{{ $month_start->format("M Y") }}</h2>
      <br>
    </div>
  </div>
  
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="row">
        @foreach($single_reports as $report)
        <div class="col-md-3">
          <div class="panel panel-default">
            <div class="panel-heading"><small>{{ $report['heading'] }}</small></div>
            <div class="panel-body text-right">{{ $report['amount'] }}</div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="row">

        <div class="col-md-12">

          <h2>{{ $daily_heading }}</h2> 

          <div class="panel panel-default">
            <table class="table table-bordered">
              <thead>
                @foreach($daily_stat as $key => $daily)
                  @if($key <= 15)
                  <th>{{$daily['day']}}</th>
                  @endif
                @endforeach
              </thead>

              <tbody><tr>
                @foreach($daily_stat as $key => $daily)
                  @if($key <= 15)
                  <td>{{$daily['number']}}</td>
                  @endif
                @endforeach
              </tr></tbody>
            </table>
          </div>


          <div class="panel panel-default">
            <table class="table table-bordered">
              <thead>
                @foreach($daily_stat as $key => $daily)
                  @if($key > 15)
                  <th>{{$daily['day']}}</th>
                  @endif
                @endforeach

                @for($i = count($daily_stat); $i < 31; $i++)
                  <th>{{$i + 1}}</th>
                @endfor
              </thead>

              <tbody><tr>
                @foreach($daily_stat as $key => $daily)
                  @if($key > 15)
                  <td>{{$daily['number']}}</td>
                  @endif
                @endforeach

                @for($i = count($daily_stat); $i < 31; $i++)
                  <td></td>
                @endfor
              </tr></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
