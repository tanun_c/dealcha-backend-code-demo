@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">

        <div class="row">
          <div class="col-xs-10 col-xs-offset-1 text-center">
            <br>
            <form action="" method="POST">
              <input type="text" class="form-control" name="search" placeholder="search">
              {!! csrf_field() !!}
              <input type="submit" name="" style="display: none;">
            </form>
            {{ $users->links() }}
            <br>
          </div>
        </div>

        @foreach($users as $user)
        <div class="row">
          <div class="col-xs-2 text-right">{{ $user->id }}</div>
          <div class="col-xs-4">{{ $user->email }}</div>
          <div class="col-xs-1">{{ count($user->cashbacks) }}</div>
          <div class="col-xs-3 small">
            <a href="/admin/users/{{ $user->id }}/edit">edit</a> | 
            <a href="/admin/users/{{ $user->id }}/cashback">cashbacks</a> 
          </div>
        </div>
        @endforeach
        <br>
      </div>
    </div>
  </div>
</div>
@endsection
