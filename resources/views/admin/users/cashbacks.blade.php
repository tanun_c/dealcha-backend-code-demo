@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      
      <div class="panel panel-default">
        <div class="row">
          <div class="col-xs-10 col-xs-offset-1">
            <small>
            <a href="/admin/users">
              Users
            </a> 
             : 
            <a href="/admin/users/{{$user->id}}/edit">
              {{$user->email}}
            </a> 
             : 
            <a href="/admin/users/{{$user->id}}/cashback">
              <strong>cashback</strong>
            </a> 
            </small>
          </div>
        </div>
      </div>
      
      <div class="panel panel-default">

        <div class="row">
          <div class="col-xs-10 col-xs-offset-1">
          <h2>Cashback entries for {{ $user->email }}</h2>
          </div>
        </div>

        
        <div class="row">
          <div class="col-xs-10 col-xs-offset-1">
            <table class="table table-striped table-condensed table-hover">
              <thead></thead> 
              <tbody>
                @foreach($cashbacks as $cashback)
                <tr>
                  <td>
                    {{ $cashback['id'] }}
                  </td>

                  <td>
                    {{ $cashback['registered'] }}
                  </td>

                  <td>
                    {{ $cashback['store_name'] }}
                  </td>

                  <td>
                    {{ $cashback['amount'] }}
                  </td>

                  <td>
                    {{ $cashback['status'] }}
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          
          </div>
        </div>
        <br>
      </div>
    </div>
  </div>
</div>
@endsection
