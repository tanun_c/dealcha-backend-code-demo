@extends('layouts.app')

@section('content')
<div class="container">
  
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">

        <table class="table table-striped table-condensed">
          <thead>
            <th class="text-right">User#</th>
            <th class="text-left">Email</th>
            {{-- <th class="text-left">Name</th> --}}
            <th class="text-left">Converted</th>
            <th class="text-left">Store</th>
            <th class="text-left">Status</th>
            <th class="text-left">Name</th>
            <th class="text-left">Account</th>
            <th class="text-right">Commission</th>
            <th class="text-right">Cashback</th>
            <th class="text-right">Revenue</th>
            <th class="text-right">Payout</th>
          </thead>
          <tbody>
            @foreach($cashbacks as $cashback)
            <tr>
              <td class="text-right">{{ $cashback->user_id }}</td>
              <td class="text-left"><small>{{ $cashback->email }}</small></td>
              {{-- <td class="text-left"><small>{{ $cashback->name }}</small></td> --}}
              <td class="text-left"><small>{{ $cashback->registered }}</small></td>
              <td class="text-left">{{ $cashback->store_name }}</td>
              <td class="text-left">{{ $cashback->status }}</td>
              
              @if ($cashback->status == 'payout' || $cashback->status == 'payout_pending')
              <td class="text-left" style="word-break: break-all;">{{ json_decode($cashback->field_value, true)['name'] }}</td>
              <td class="text-left" style="word-break: break-all;">{{ json_decode($cashback->field_value, true)['acct_no'] }} {{ json_decode($cashback->field_value, true)['acct_bank'] }}</td>
              @else
              <td></td>
              <td></td>
              @endif
              {{-- <td class="text-left"></td> --}}

              @if($cashback->status != "payout")
                <td class="text-right">{{ $cashback->conversion }}</td>
                <td class="text-right">{{ $cashback->cashback }}</td>
                <td class="text-right">{{ number_format($cashback->conversion - $cashback->cashback, 2) }}</td>
                <td></td>
              @else
                <td></td>
                <td></td>
                <td></td>
                <td class="text-right">{{ $cashback->cashback }}</td>
              @endif
            </tr>
            @endforeach
          </tbody>
        </table>

      </div>
    </div>
  </div>
</div>
@endsection
