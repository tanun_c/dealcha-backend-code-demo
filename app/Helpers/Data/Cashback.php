<?php
namespace App\Helpers\Data;

class Cashback {
  
  public function normal ($start, $end, $store = null)
  {
    $model_cashback = app('App\Models\Cashback');

    if (empty($store))
    {
      $all_cb = $model_cashback
        ->where('registered', '<=', $end)
        ->where('registered', '>', $start)
        ->whereIn('status', ['pending', 'ready', 'approved'])
        ->with('conversion')
        ->get();
    }
    else
    {
      $all_cb = $model_cashback
        ->where('registered', '<=', $end)
        ->where('registered', '>', $start)
        ->where('store_id', $store)
        ->whereIn('status', ['pending', 'ready', 'approved'])
        ->with('conversion')
        ->get();
    }

    return $all_cb;
  }

}