<?php
namespace App\Helpers\Data;

class DailyRegisterUser {
  
  public function month ($month_start)
  {
    $month_end = $month_start->copy()->addMonth();

    $current_date = $month_start;

    $result = [];

    while ($current_date->lt($month_end))
    {
      $day_result = [];
      $day_result['day'] = $current_date->day;

      $tomorrow = $current_date->copy()->addDay();

      $day_result['number'] = app('App\Models\DealchaUser')
        ->where('created_at', '<=', $tomorrow)
        ->where('created_at', '>', $current_date)
        ->count();

      $current_date = $tomorrow;
      $result[] = $day_result;
    }

    return $result;
  }

}