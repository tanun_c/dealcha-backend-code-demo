<?php
namespace App\Helpers\Data;

use Carbon\Carbon;

// App\Helpers\Data\CohortConversion
class CohortConversion {

  public function __construct ()
  {
    $this->user_model = app('App\Models\DealchaUser');
    $this->cashback_model = app('App\Models\Cashback');
  }

  public function calculate ()
  {
    $today = Carbon::now();

    $month_start = $today->copy()->subMonth();
    $current_date = $month_start;

    $result = [];

    while ($current_date->lt($today))
    {
      $day_result = [];
      $day_result['day'] = $current_date->format('d');

      $tomorrow = $current_date->copy()->addDay();

      // Get the user who register around this date
      $users_register = $this->user_model
                          ->where('created_at', '<', $tomorrow)
                          ->where('created_at', '>', $current_date)
                          ->get();

      $num_user_register = count($users_register);
      $ids_user_register = array_pluck($users_register->toArray(), 'id');

      // Load conversions considering cashback entries
      $conversions = $this->cashback_model
                        ->whereIn('user_id', $ids_user_register)
                        ->whereNotIn('status', ['payout', 'payout_pending', 'bonus'])
                        ->get();

      // Get user IDs from conversion
      $conversions_user_ids = array_pluck($conversions->toArray(), 'user_id');
      $conversions_uniq_user_ids = array_unique($conversions_user_ids);

      // Add data to day result
      $day_result['count'] = count($conversions_uniq_user_ids);
      $result[] = $day_result;

      $current_date = $tomorrow;
    }

    return $result;
  }
}