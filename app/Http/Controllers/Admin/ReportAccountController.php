<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class ReportAccountController extends Controller
{
  public function show ($year, $month)
  {
    if ($month == 'all')
    {
      $month_start = Carbon::now();
      $month_start->year = $year;
      $month_start->month = 1;
      $month_start->day = 1;
  
      $month_end = $month_start->copy()->addYear();
    }
    else
    {
      $month_start = Carbon::now();
      $month_start->year = $year;
      $month_start->month = $month - 1;
      $month_start->day = 1;
  
      $month_end = $month_start->copy()->addMonth();
    }

    $cashbacks = app('db')
      ->table('user_cashback')
      ->leftJoin('conversions', 'conversions.id', '=', 'user_cashback.conversion_id')
      ->leftJoin('users', 'users.id', '=', 'user_cashback.user_id')
      ->leftJoin('stores', 'stores.id', '=', 'user_cashback.store_id')
      ->leftJoin('info_fields', 'users.id', '=', 'info_fields.user_id')
      ->select(
          'conversions.amount as conversion', 
          'user_cashback.amount as cashback', 'user_cashback.registered', 'user_cashback.diff_id', 
          'users.email', 
          'users.name', 
          'user_cashback.user_id', 'stores.name as store_name', 'user_cashback.status', 'info_fields.field_value')
      ->where('user_cashback.registered', '>=', $month_start)
      ->where('user_cashback.registered', '<', $month_end)
      ->whereIn('user_cashback.status', ['pending', 'approved', 'payout'])
      ->get();

    $sum_cashback = app('db')
      ->table('user_cashback')
      ->leftJoin('conversions', 'conversions.id', '=', 'user_cashback.conversion_id')
      ->where('user_cashback.registered', '>=', $month_start)
      ->where('user_cashback.registered', '<', $month_end)
      ->whereIn('user_cashback.status', ['pending', 'approved'])
      ->sum('user_cashback.amount');

    $sum_conversion = app('db')
      ->table('user_cashback')
      ->leftJoin('conversions', 'conversions.id', '=', 'user_cashback.conversion_id')
      ->where('user_cashback.registered', '>=', $month_start)
      ->where('user_cashback.registered', '<', $month_end)
      ->whereIn('user_cashback.status', ['pending', 'approved'])
      ->sum('conversions.amount');
    
    return view('reports.account', compact('cashbacks'));
  }
}
