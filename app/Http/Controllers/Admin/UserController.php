<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index(Request $request)
  {
    $baseQuery = app('App\Models\DealchaUser')->newQuery();

    $search = $request->input('search', false);
    if ($search !== false) $baseQuery->where('email', 'like', "%{$search}%");

    $users = $baseQuery->paginate(100);
    return view('admin.users.index', compact('users'));
  }

  public function show (Request $request, $id)
  {
    $user = app('App\Models\DealchaUser')->find($id);
    return view('admin.users.show', compact('user'));
  }

  public function update (Request $request, $id)
  {
    $user = app('App\Models\DealchaUser')->find($id);
    $password = $request->input('password');

    $user->password = \Hash::make($password);
    $user->save();

    return view('admin.users.show', compact('user'));
  }

  public function cashbacks ($id)
  {
    $user = app('App\Models\DealchaUser')->find($id);
    $cashbacks = $user->cashbacks()->orderBy('registered', 'desc')->get()->toArray();
    return view('admin.users.cashbacks', compact('user', 'cashbacks'));
  }
}
