<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use DB;

class DashboardController extends Controller
{
  public function dashboard (Request $req)
  {
    $store = $req->input('store', null);

    $model_transaction = app('App\Models\Transaction');
    $model_user = app('App\Models\DealchaUser');
    $model_cashback = app('App\Models\Cashback');
    $model_conversion = app('App\Models\Conversion');
    $model_store = app('App\Models\Store');

    $stores = $model_store
                ->whereHas('cashbacks')
                ->where('id', '<>', 0)
                ->orderBy('is_featured', 'desc')
                ->get();

    // Retrieve cashback per month

    // Retrieve trips per month
    $months = $this->months();

    $months_number_data = [];
    
    for ($i = 0; $i < 11; $i++)
    {
      if (empty($store))
      {
        $trips_this_month = $model_transaction
          ->where('occured', '<=', $months[$i])
          ->where('occured', '>', $months[$i + 1])
          ->count();
      }
      else
      {
        $trips_this_month = $model_transaction
          ->where('occured', '<=', $months[$i])
          ->where('occured', '>', $months[$i + 1])
          ->where('store_id', $store)
          ->count();
      }

      $new_user_this_month = $model_user
        ->where('created_at', '<=', $months[$i])
        ->where('created_at', '>', $months[$i + 1])
        ->count();

      $all_cb_this_month = app('App\Helpers\Data\Cashback')
        ->normal($months[$i + 1], $months[$i], $store);

      $cashback_this_month = $all_cb_this_month->sum('amount');

      $pending_cashback_this_month = $all_cb_this_month->filter(function($item){
        return $item->status == 'pending'; 
      })->sum('amount');

      $approved_cashback_this_month = $all_cb_this_month->filter(function($item){
        return $item->status == 'approved'; 
      })->sum('amount');

      $rev_this_month = $all_cb_this_month->sum('conversion.amount');
      $pending_rev_this_month = $all_cb_this_month->filter(function($item){
        return $item->status == 'pending'; 
      })->sum('conversion.amount');

      $approved_rev_this_month = $all_cb_this_month->filter(function($item){
        return $item->status != 'pending'; 
      })->sum('conversion.amount');

      $all_bonus_this_month = $model_cashback
        ->where('registered', '<=', $months[$i])
        ->where('registered', '>', $months[$i + 1])
        ->whereIn('status', ['bonus'])
        ->get();

      $bonus_this_month = $all_bonus_this_month->sum('amount');

      $all_payout_this_month = $model_cashback
        ->where('registered', '<=', $months[$i])
        ->where('registered', '>', $months[$i + 1])
        ->whereIn('status', ['payout'])
        ->get();

      $payout_this_month = $all_payout_this_month->sum('amount');

      $months_number_data[] = [
        'month' => $months[$i + 1]->format('M Y'), 
        'href'  => '/admin/dashboard/' . strtolower($months[$i + 1]->format('Y/M')), 
        'trips' => $trips_this_month,
        'users' => $new_user_this_month,
        'cashback' => $cashback_this_month,
        'cb_approved' => $approved_cashback_this_month,
        'cb_pending' => $pending_cashback_this_month,
        'revenue' => $rev_this_month,
        'revenue_pending' => $pending_rev_this_month,
        'revenue_approved' => $approved_rev_this_month,
        'bonus' => $bonus_this_month,
        'payout' => $payout_this_month
      ];
    }


    $single_reports = [];

    $total_user = $model_user->count();
    $single_reports[] = [
      'heading' => "Total users <br> &nbsp;", 
      "amount" => number_format($total_user)
    ];

    $user_one_trip = $model_transaction
        ->select('user_id')
        ->distinct()->get();

    $percent = number_format(100 * (count($user_one_trip) / $total_user), 1);

    $single_reports[] = [
      'heading' => "User that had at least 1 trip in lifetime", 
      "amount" => number_format(count($user_one_trip)) . " ({$percent}%)"
    ];

    $user_one_cashback = $model_cashback
        ->whereIn('status', ['pending', 'approved'])
        ->select('user_id')
        ->distinct()->get();

    $percent = number_format(100 * (count($user_one_cashback) / $total_user), 1);

    $single_reports[] = [
      'heading' => "User that had at least 1 cashback in lifetime", 
      "amount" => number_format(count($user_one_cashback)) . " ({$percent}%)"
    ];

    return view('admin.dashboard', compact('months_number_data', 'single_reports', 'stores'));
  }

  public function monthly ($year, $month)
  {
    $model_transaction = app('App\Models\Transaction');
    $model_user = app('App\Models\DealchaUser');
    $model_cashback = app('App\Models\Cashback');

    $month_start  = new Carbon("{$year}-{$month}-1 00:00:00");
    $month_end    = $month_start->copy()->addMonth();

    $single_reports = [];

    $user_one_trip = $model_transaction
        ->where('occured', '<=', $month_end)
        ->where('occured', '>', $month_start)
        ->select('user_id')
        ->distinct()->get();

    $single_reports[] = [
      'heading' => "Users that had at least 1 trip in this month", 
      "amount" => number_format(count($user_one_trip))
    ];

    $user_one_cashback = $model_cashback
        ->where('registered', '<=', $month_end)
        ->where('registered', '>', $month_start)
        ->whereIn('status', ['pending', 'approved'])
        ->select('user_id')
        ->distinct()->get();

    $single_reports[] = [
      'heading' => "Users that had at least 1 cashback in this month", 
      "amount" => number_format(count($user_one_cashback))
    ];

    $user_has_cashback = array_pluck($user_one_cashback, 'user_id');

    $user_cashback_before = $model_cashback
      ->whereIn('user_id', $user_has_cashback)
      ->whereIn('status', ['pending', 'approved'])
      ->where('registered', '<', $month_start)
      ->select('user_id')
      ->distinct()->get();

    $single_reports[] = [
      'heading' => "Users that had the first cashback in this month", 
      "amount" => number_format(count($user_has_cashback) - count($user_cashback_before))
    ];


    $user_with_new_lazada = $model_cashback
      ->whereIn('status', ['pending', 'approved'])
      ->where('registered', '<=', $month_end)
      ->where('registered', '>', $month_start)
      ->where('store_id', 1)
      ->where('is_lazada_new_user', 1)
      ->select('user_id')
      ->distinct()->get();


    $user_with_lazada = $model_cashback
      ->whereIn('status', ['pending', 'approved'])
      ->where('registered', '<=', $month_end)
      ->where('registered', '>', $month_start)
      ->where('store_id', 1)
      ->select('user_id')
      ->distinct()->get();

    if (count($user_with_lazada) != 0) {
      $percent = number_format(100 * count($user_with_new_lazada) / count($user_with_lazada), 1);
    }
    else
    {
      $percent = '-';
    }

    $single_reports[] = [
      'heading' => "Users who just registered with Lazada", 
      "amount" => number_format(count($user_with_new_lazada)) . " ({$percent}%)"
    ];

    $single_reports[] = [
      'heading' => "% Active Users", 
      "amount" => number_format(count($user_one_cashback) * 100 / $model_user->count(), 1) . "%"
    ];

    $daily_heading = "Daily Registered User";
    $daily_stat = app('App\Helpers\Data\DailyRegisterUser')->month($month_start);

    return view('admin.dashboard-monthly', compact('month_start', 'single_reports', 'daily_stat', 'daily_heading'));
  }

# columns do

    #   column do
    #     panel '% Total Active Users' do
    #       User
    #         .includes(:cashback)
    #         .where.not({ user_cashback: { user_id: nil } })
    #         .count * 100 / User.count
    #     end
    #   end
    # end

    # columns do
    #   column do
    #     panel "Monthly Summary" do

    #       # Get the last 12 months
    #       months = (0..11).map{|i| (Date.today - i.month).beginning_of_month}

    #       monthly_value = controller.monthly_stat(months)
    #       monthly_value.each do |value|
    #         if value[:trips_num].to_i.zero? &&
    #            value[:cashback_sum].to_i.zero? &&
    #            value[:approved_revenue_sum].to_i.zero? &&
    #            value[:pending_revenue_sum].to_i.zero? &&
    #            value[:bonus_sum].to_i.zero? &&
    #            value[:payout_sum].to_i.zero? &&
    #            value[:order_sum].to_i.zero? &&
    #            value[:order_uniq_sum].to_i.zero? &&
    #            value[:user_number].to_i.zero?

    #            monthly_value.delete(value)
    #         end
    #       end

    #       table_for monthly_value do
    #         column(:month) { |month| "#{month[:month].strftime('%b %Y')}" }
    #         column(:trips, :trips_num)
    #         column(:cashback, :cashback_sum)
    #         column(:approved_revenue, :approved_revenue_sum)
    #         column(:pending_revenue, :pending_revenue_sum)
    #         column(:bonus, :bonus_sum)
    #         column(:payout, :payout_sum)
    #         column(:order, :order_sum)
    #         column(:uniq_order, :order_uniq_sum)
    #         column(:new_users, :user_number)
    #       end # table

    #     end # panel
    #   end
    # end

    # # New update and reminder part
    # columns do
    #   column do
    #     panel "Recent Shopping Trips" do
    #       table_for Trip.last(25).reverse do
    #         column :user
    #         column :store
    #         column :date, :occured
    #       end # table
    #     end # panel
    #   end # column

    #   column do
    #     panel "Pending Payout Requests" do
    #       table_for Cashback.where('status = "payout_pending"').reverse do |cashback|
    #         column(:user)
    #         column(:amount)
    #         column(:registered)
    #         column 'Action' do |cashback|
    #           link_to 'Edit', edit_admin_cashback_path(cashback)
    #         end
    #       end # table
    #     end # panel

    #     panel 'User Ranking' do
    #       @users_cashback = User
    #                           .joins(:cashback)
    #                           .where.not({ user_cashback: { status: %w(bonus payout pending_payout) } })
    #                           .select('users.email as email, sum(user_cashback.amount) as cashback_amount')
    #                           .group('email').map { |m| { email: m.email, cashback_amount: m.cashback_amount.to_f } }
    #       @users_payout = User
    #                         .joins(:cashback)
    #                         .where({ user_cashback: { status: %w(payout) } })
    #                         .select('users.email as email, sum(user_cashback.amount) as payout_amount')
    #                         .group('email').map { |m| { email: m.email, payout_amount: m.payout_amount.to_f } }

    #       @users = (@users_cashback + @users_payout)
    #                  .group_by { |user| user[:email] }
    #                  .map { |k, v| v.reduce(:merge) }
    #                  .sort_by { |user| [-(user[:cashback_amount] || 0), -(user[:payout_amount] || 0)] }

    #       if params[:order] && @users.first[params[:order].gsub('_desc', '').to_sym] != nil
    #         @users = @users.sort_by { |user| -(user[params[:order].gsub('_desc', '').to_sym] || 0) }
    #       end

    #       table_for @users.first(10), sortable: :true do
    #         column(:email)
    #         column(:cashback_amount) { |user| number_with_delimiter(user[:cashback_amount]) }
    #         column(:payout_amount) { |user| number_with_delimiter(user[:payout_amount]) }
    #       end
    #     end

    #     panel 'Store Ranking' do
    #       @stores = Store
    #                   .all
    #                   .map { |s| { name: s.name, shopping_trips: s.trips.count, cashbacks: s.cashbacks.where.not(status: %w(bonus)).sum(:amount) } }
    #                   .sort_by { |store| [-store[:shopping_trips], -store[:cashbacks]] }

    #       if params[:order] && @stores.first[params[:order].gsub('_desc', '').to_sym]
    #         @stores = @stores.sort_by { |s| -(s[params[:order].gsub('_desc', '').to_sym] || 0) }
    #       end

    #       table_for @stores.first(10), sortable: :true do
    #         column(:name)
    #         column(:shopping_trips) { |store| number_with_delimiter(store[:shopping_trips]) }
    #         column(:cashbacks) { |store| number_with_delimiter(store[:cashbacks]) }
    #       end
    #     end
    #   end # column

    # end # columns

  public function months ()
  {
    $now = new Carbon();

    $result = [$now];

    $this_month = $now->copy();
    $this_month->day = 1;
    $this_month->hour = 0;
    $this_month->minute = 0;
    $this_month->second = 0;

    $new_month = $this_month;
    $result[] = $new_month;

    for ($i = 0; $i < 11; $i++) 
    { 
      $new_month = $new_month->copy();
      $new_month->subMonth();
      $result[] = $new_month;
    }
    
    return $result;
  }
}
