<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;

class CohortController extends Controller
{
  public function convert (Request $req)
  {
    $conversion_stat = app('App\Helpers\Data\CohortConversion')->calculate();
    return view('admin.cohort.conversion', compact('conversion_stat'));
  }
}