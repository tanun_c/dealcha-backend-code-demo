<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cashback extends Model
{
  protected $table = 'user_cashback';
  protected $appends = ['store_name'];

  public function conversion ()
  {
    return $this->belongsTo('App\Models\Conversion', 'conversion_id');
  }

  public function store ()
  {
  	return $this->belongsTo('App\Models\Store', 'store_id');
  }

  public function getStoreNameAttribute ($val)
  {
  	return $this->store->name;
  }
}