<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
  protected $table = 'stores';

  public function cashbacks ()
  {
  	return $this->hasMany('App\Models\Cashback', 'store_id');
  }
}