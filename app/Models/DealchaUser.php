<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DealchaUser extends Model
{
  protected $table = 'users';

  public function cashbacks ()
  {
  	return $this->hasMany('App\Models\Cashback', 'user_id');
  }
}