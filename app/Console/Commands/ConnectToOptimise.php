<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Log;

use App\Models\Cashback;
use App\Models\Conversion;
use App\Models\Transaction;

class ConnectToOptimise extends Command
{
  protected $signature = 'connect:optimise';
  protected $description = 'Command description';

  public function __construct()
  {
      parent::__construct();
  }

  public function handle()
  {
    $this->info('optimise');
    
    date_default_timezone_set("UTC"); 
    $t = microtime(true); 
    $micro = sprintf("%03d",($t - floor($t)) * 1000); 
    $utc = gmdate('Y-m-d H:i:s.', $t).$micro;  
    $sig_data= $utc;

    $private_key = env('OPTIMISE_PRIVATE_KEY');
    $concateData = $private_key.$sig_data;
    $sig = md5($concateData);
    $api_key = env('OPTIMISE_API_KEY');

    $url = "http://api.omgpm.com/network/OMGNetworkApi.svc/v1.2/Reports/Affiliate/TransactionsOverview";
    $query = array('Key' => $api_key, 'Sig' => $sig, 'SigData' => $sig_data, 'AgencyID' => 118, 'Status' => -1, 'AID' => 845818, 'StartDate' => (new Carbon())->subMonth(4)->toDateString(), 'EndDate' => (new Carbon())->toDateString());

    $response = app('GuzzleHttp\Client')->get($url, ['query' => $query]);
    $response_json = $response->getBody();
    $response_array = json_decode($response_json, true);

    foreach ($response_array as $key => $response_row) 
    {
      $time = $response_row['TransactionTime'];
      $time = str_replace('/Date(', '', $time);
      $time = str_replace(')', '', $time);
      $time = explode('+', $time)[0] / 1000;

      $registered = new Carbon;
      $registered->setTimestamp($time);

      $cutoff_90_days     = $registered->copy()->addDays(90);
      $more_than_90_days  = $cutoff_90_days->lt(Carbon::today());

      $transaction_value_USD  = $response_row['TransactionValue'];
      $reward_value_USD       = $response_row['NVR'];
      $transaction_identifier = $response_row['TransactionID'];
      $dealcha_transaction_id = $response_row['UID'];
      $status = $response_row['Status'];
      $status = lcfirst($status);
      if ($status == 'validated') $status = 'approved';

      // Load transaction
      $transaction = Transaction::find($dealcha_transaction_id)->toArray();

      $existing_conversion = Conversion::where('diff_id', $transaction_identifier)->where('trip_id', $dealcha_transaction_id)->first();
      if (empty($existing_conversion))
      {
        $conversion = new Conversion;
        $conversion->trip_id = $transaction['id'];
        $conversion->diff_id = $transaction_identifier;
        $conversion->purchase = $transaction_value_USD * 34;
        $conversion->amount = $reward_value_USD * 34;
        $conversion->status = $status;
        $conversion->save();
      }

      if(empty($transaction['user_id'])) continue;

      $existing_cashback = Cashback::where('diff_id', $transaction_identifier)->where('transaction_id', $dealcha_transaction_id)->first();

      if (empty($existing_cashback))
      {
        Log::info("OPTIMISE_CONNECTION: Creating new cashback entry");
        $cashback = new Cashback;

        $cashback->user_id  = $transaction['user_id'];
        $cashback->status  = $status;
        $cashback->registered = $registered;
        $cashback->store_id = $transaction['store_id'];
        $cashback->transaction_id = $transaction['id'];
        $cashback->diff_id = $transaction_identifier;
        $cashback->purchase_amount = $transaction_value_USD * 34;
        $cashback->amount = $reward_value_USD * 34 / 2;
        $cashback->save();
      }
      elseif ($more_than_90_days)
      {
        Log::info("OPTIMISE_CONNECTION: Changing existing cashback status");
        $cashback = $existing_cashback;
        $cashback->status = $status;
        $cashback->save();
      }
      else
      {
        Log::info("OPTIMISE_CONNECTION: Skipped changing cashback status due to less than 90 days");
        continue;
      }
    }
  }
}
