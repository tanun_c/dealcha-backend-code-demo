<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['prefix' => 'admin', 'middleware' => 'auth', 'namespace' => 'Admin'], function (){
  Route::get('/dashboard', 'DashboardController@dashboard');
  Route::get('/dashboard/{year}/{month}', 'DashboardController@monthly');
  Route::get('/users', 'UserController@index');
  Route::post('/users', 'UserController@index');
  Route::get('/users/{id}/cashback', 'UserController@cashbacks');
  Route::get('/users/{id}', 'UserController@show');
  Route::put('/users/{id}', 'UserController@update');

  Route::get('/report/account/{year}/{month}', 'ReportAccountController@show');
  Route::get('/report/cohort/convert', 'CohortController@convert');
});